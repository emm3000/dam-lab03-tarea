import React, { useState } from 'react'

import { TextInput, View, Text, Image, Button } from 'react-native'


export default ({ navigation }) => {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text style={{paddingBottom: 15}}>Components:</Text>
      <Button
        title="Age Validator"
        onPress={() => navigation.navigate('Validator')}
      />
      <View
          style={{
            borderBottomColor: '#A9A9A9',
            borderBottomWidth: 1,
            paddingBottom: 10
          }}
        />
      <Button
        title="Flat List"
        onPress={() => navigation.navigate('List')}
      />
    </View>
  )
}
