import React, { useState, useEffect } from 'react'
import { View, Text, Button, Image } from 'react-native'

export default ({ navigation, route }) => {

  useEffect(() => {
    navigation.setParams({ title: route.params.title })
  }, [])
  return (
    <View style={{
      alignItems: 'center',
    }}>
      <View style={{
        width: 300,
        height: 300,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#009688',
        marginTop: 20
      }}>
        <Image
          source={{
            uri: `https://pokeres.bastionbot.org/images/pokemon/${route.params.image}.png`
          }}
          style={{
            width: '100%',
            height: null,
            flex: 1,
            resizeMode: 'contain'
          }}
        />
      </View>
      <View style={{
        width: 300,
        height: 190,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#009688',
        marginTop: 20,
        padding: 15
      }}>
        <Text>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam, nam quo! Corrupti, reiciendis recusandae nobis ut et nostrum neque vero possimus? Necessitatibus ex ipsa tenetur aperiam modi perferendis</Text>
      </View>

    </View>
  )
}