import React, { useState, useEffect } from 'react'

import {
  TextInput,
  View,
  Text,
  FlatList,
  Image,
  Button,
  TouchableOpacity
} from 'react-native'
import Item from '../item/item'

{/* <Image source={{
          uri: 'https://pokeres.bastionbot.org/images/pokemon/101.png',
        }}
        style={{
          width: 150,
          height: 150,
        }}/> */}

// const DATA = [
//   {
//     "id": 1,
//     "name": "bulbasaur",
//     "url": "https://pokeapi.co/api/v2/pokemon/1/"
//   },
//   {
//     "id": 2,
//     "name": "ivysaur",
//     "url": "https://pokeapi.co/api/v2/pokemon/2/"
//   },
//   {
//     "id": 3,
//     "name": "venusaur",
//     "url": "https://pokeapi.co/api/v2/pokemon/3/"
//   },
//   {
//     "id": 4,
//     "name": "charmander",
//     "url": "https://pokeapi.co/api/v2/pokemon/4/"
//   },
//   {
//     "id": 5,
//     "name": "charmeleon",
//     "url": "https://pokeapi.co/api/v2/pokemon/5/"
//   },
//   {
//     "id": 6,
//     "name": "charizard",
//     "url": "https://pokeapi.co/api/v2/pokemon/6/"
//   },
//   {
//     "id": 7,
//     "name": "squirtle",
//     "url": "https://pokeapi.co/api/v2/pokemon/7/"
//   },
//   {
//     "id": 8,
//     "name": "wartortle",
//     "url": "https://pokeapi.co/api/v2/pokemon/8/"
//   },
//   {
//     "id": 9,
//     "name": "blastoise",
//     "url": "https://pokeapi.co/api/v2/pokemon/9/"
//   },
//   {
//     "id": 10,
//     "name": "caterpie",
//     "url": "https://pokeapi.co/api/v2/pokemon/10/"
//   },

// ]




export default ({ navigation }) => {
  const [data, setData] = useState([])

  useEffect(() => {
    fetch('https://pokeapi.co/api/v2/pokemon?limit=100')
      .then(response => response.json())
      .then(json => setData(json.results))

  }, [])
  // paddingBottom
  return (
    <View style={{ padding: 5 }}>
      <FlatList
        data={data}
        renderItem={({ item, index }) => (
          <Item
            title={item.name[0].toUpperCase() + item.name.slice(1)}
            imagen={index + 1}
            navigation={navigation}
          />)
        }
        keyExtractor={item => item.name}
        ItemSeparatorComponent={() => <View
          style={{
            borderBottomColor: '#A9A9A9',
            borderBottomWidth: 1,
          }}
        />}
      />

    </View>

  );
}