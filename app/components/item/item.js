import React from 'react'
import {
  View,
  TouchableOpacity,
  Text,
  Image
} from 'react-native'
//comentario para origin master
console.log("itemflatlist")
export default React.memo(({ title, imagen, navigation }) => {
 
  return (
    <View>
      <TouchableOpacity
        onPress={() => navigation.navigate('Details', { image: imagen, title })}
      >
        <View style={{
          //backgroundColor: 'green',
          padding: 10,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
          <View style={{
            width: 120,
            height: 120,
            borderRadius: 10,
            borderWidth: 2,
            borderColor: '#009688',
            alignItems: 'center',
          }}>
            <Image source={{
              uri: `https://pokeres.bastionbot.org/images/pokemon/${imagen}.png`
            }}
              style={{
                width: '100%',
                height: null,
                flex: 1,
                resizeMode: 'center'
              }} />
          </View>
          <View style={{
            //backgroundColor: 'blue',
            width: 120,
            height: 120,
            flexGrow: 1,
            paddingLeft: 10,
            flexDirection: 'column',

          }}>
            <Text style={{
              fontSize: 15,
              fontWeight: "bold",
            }}>{title}</Text>
            <Text style={{
              fontSize: 12,
              alignContent: 'center',
              //backgroundColor: 'pink',
              justifyContent: 'center',
              flexGrow: 1
            }}>{"\n"}Lorem Ipsum is simply dummy text of the printing and typesetting industry </Text>
            <Text style={{
              fontSize: 11,
              marginBottom: 13
            }}>{title}</Text>
          </View>
        </View>

      </TouchableOpacity>
    </View>

  );
})