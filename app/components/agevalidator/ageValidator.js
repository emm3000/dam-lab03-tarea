import React, { useState } from 'react'

import { TextInput, View, Text, Image, Button } from 'react-native'

const AgeValidator = ({ navigation }) => {
  const [edad, setEdad] = useState("")

  
  const updateEdad = (text) => {
    //let test = !!text
    //console.log(test)
    // fetch('https://jsonplaceholder.typicode.com/todos/1')
    //   .then(response => response.json())
    //   .then(json => console.log(json))
    setEdad(text)
  }

  return (
    <View style={{padding: 20}}>
      <Text style={{ marginBottom: 3 }}>Ingrese su edad:</Text>   
      <TextInput
        value={edad}
        onChangeText={updateEdad}
        keyboardType='numeric'
        autoFocus
        style={
          {
            textAlign: 'center',
            height: 40,
            borderRadius: 10,
            borderWidth: 2,
            borderColor: '#009688',
            marginBottom: 10
          }
        }
      />
      {
        !!edad && <Text>Rpta: {parseInt(edad) >= 18 ? "Mayor de edad" : "Menor de edad"}</Text>
      }
       <Button
        title="To go list"
        onPress={() => navigation.navigate('List')}
      />
    </View>
  )
}

export default AgeValidator