
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button
} from 'react-native';

import Validator from './app/components/agevalidator/ageValidator'
import List from './app/components/mylist/myList'
import Navigation from './app/components/navigation/navigation'
import Details from './app/components/details/details'
//import Rutas from './app/components/routertest/Routing'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();


//initialParams={{ itemId: 42 }} 
const App = () => {
  return (

    <NavigationContainer style={{ backgroundColor: 'red' }}>

      <Stack.Navigator initialRouteName="Navigation" >
        <Stack.Screen name="Navigation" component={Navigation} options={{ headerShown: true }}/>
        <Stack.Screen name="Validator" component={Validator} />
        <Stack.Screen name="List" component={List} />
        <Stack.Screen name="Details" component={Details} options={({ route }) => ({ title: route.params.title })}/>

      </Stack.Navigator>

    </NavigationContainer>



  )
}

export default App;
